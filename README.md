# brte
BRTE ROS tools package

**Author:** [David Zuñiga-Noël](http://mapir.isa.uma.es/dzuniga)

**License:**  [GPLv3](LICENSE.txt)

## 1. Dependencies (tested)
* CMake (3.5.1-1ubuntu1)
   ```
   sudo apt install cmake
   ```
* ROS (Kinetic Kame with catkin workspace)

   See [ROS installation](http://wiki.ros.org/kinetic/Installation) and [catkin documentation](http://wiki.ros.org/catkin).

* Boost (1.58.0.1ubuntu1)
   ```
   sudo apt install libboost-all-dev
   ```
* Gflags (2.1.2-3)
   ```
   sudo apt install libgflags-dev
   ```
* OpenCV (from ROS)

Any missing ROS package can be installed with the distribution's package manager. For example, the `geographic_msgs` can be installed by running:
```
sudo apt install ros-kinetic-geographic-msgs
```
   
## 2. Build

Once all dependencies are installed, proceed to build the source code within a catkin workspace (see [catkin tutorials](http://wiki.ros.org/catkin/Tutorials)).

## 3. Data Format

### 3.1 `truth_file`
The input groundtruth file should be a numeric table, with whitespace separated numeric values:
```
t_sec phi_rad lambda_rad h_m q_nb0 q_nb1 q_nb2 q_nb3
```
where

* `t_sec`: specifies the time offset in seconds (relative to the time base).

* `phi_rad`: latitude, in radians.

* `lambda_rad`: longitude, in radians.

* `h_m`: altitude, in meters (WGS84 ellipsode at h=0).

* `q_nbi`: orientation relative to the local NED frame as a hamiltonian quaternion [(q_w, q_x, q_y, q_z) = (q_nb0, q_nb1, q_nb2, q_nb3)]

**Note:** any line containing non-numeric values should be commented out by prepending a pound `#`.

### 3.2 `sens_file`
The input inertial file should be a numeric table, with whitespace separated numeric values:
```
t_sec ax_mps2 ay_mps2 az_mps2 wx_rps wy_rps wz_rps
```
where

* `t_sec`: specifies the time offset in seconds (relative to the time base).

* `a{x,y,z}_mps2`: three axis linear acceleration, in meters/second^2.

* `w{x,y,z}_rps`: three axis angular velocity, in radians/second.

**Note:** any line containing non-numeric values should be commented out by prepending a pound `#`.

### 3.3 `images_file`
The input images file should be a numeric table, with whitespace separated numeric values:
```
id t_sec phi_rad lambda_rad h_m psi_rad theta_rad xi_rad saved
```
where

* `id`: image id, from which the image filename follows.

* `t_sec`: specifies the time offset in seconds (relative to the time base).

The remaining fields are ignored but they have to be present to correctly parse the file in memory.

**Note:** any line containing non-numeric values should be commented out by prepending a pound `#`.

### 3.4 `images_dir`
The images directory should be contain the image files, named as:
```
optional_name_id.ext
```
where the id is a unique numeric image identifier that maches the `id` filed in `images_file`.

## 4. Usage

The `bag_creater` tool can invoked as follows:
```
rosrun brte bag_creater [options] <truth_file> <sens_file> <images_file> <images_dir> <output>
```
where the available options are:

* `--grayscale`: flag to specify whether or not to convert images to grayscale (saves space).

* `--time_origin=new_time`: specifies the new time base, in seconds. Negative values set it to NOW.

* `--help`: shows usage information.
