
#define PROGRAM_NAME \
    "asscoiate"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, rosbag_start, 0.0, "Rosbag start time [s]")                                  \
    FLAG_CASE(string, images_output, "", "Output file for images associated")                      \
    FLAG_CASE(string, rosbag_output, "", "Output file for rosbag associated")                      \
    FLAG_CASE(bool, save_corrected, false, "Save corrected rosbag timestamps")

#define ARGS_CASES                                                                                 \
    ARG_CASE(images_data)                                                                          \
    ARG_CASE(rosbag_data)

// STL
#include <cmath>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>

// ROS
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <geographic_msgs/GeoPoseStamped.h>

#include "util/macros.h"
#include "util/version.h"

#include "associate.hpp"
#include "csv.hpp"

namespace fs = boost::filesystem;

inline std::string to_string(const double n, const int precision = 9) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(precision) << n;
    return ss.str();
}

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_images_data));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_rosbag_data));
}

inline void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_rosbag_start > 0.0);
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();
    
    // Read ARGS_input1 (csv with ' ' separator)
    Eigen::MatrixXd images_data = csv::read<double>(ARGS_images_data, ' ');
    // #id t_sec phi_rad lambda_rad h_m psi_rad theta_rad xi_rad saved
    RUNTIME_ASSERT(images_data.cols() == 9);
    
    std::vector<double> images_timestamps(images_data.rows());
    for (int i = 0; i < images_data.rows(); ++i)
        images_timestamps[i] = images_data(i, 1);
    
    // Read ARGS_input2 (csv with ' ' separator)
    Eigen::MatrixXd rosbag_data = csv::read<double>(ARGS_rosbag_data, ' ');
    // #t_sec phi_rad lambda_rad h_m
    RUNTIME_ASSERT(rosbag_data.cols() == 4);
    
    std::vector<double> rosbag_timestamps(rosbag_data.rows());
    for (int i = 0; i < rosbag_data.rows(); ++i)
        rosbag_timestamps[i] = rosbag_data(i, 0) - FLAGS_rosbag_start; // rosbag start time
    
    std::vector<std::pair<std::size_t, std::size_t>> pairs = associate(images_timestamps, rosbag_timestamps, 0.001);
    const int N = pairs.size();
    
    Eigen::MatrixXd associated;
    static_assert(std::numeric_limits<double>::has_quiet_NaN);
    
    if (!FLAGS_images_output.empty()) {
        associated.setConstant(N, 4, std::numeric_limits<double>::quiet_NaN());
        for (int i = 0; i < N; ++i) {
            std::size_t images_idx = pairs[i].first;
            
            associated.row(i) = images_data.block<1, 4>(images_idx, 1);
        }
            
        const bool saved_images_output = csv::write(associated, FLAGS_images_output, 9);
        RUNTIME_ASSERT(saved_images_output);
    }
    
    if (!FLAGS_rosbag_output.empty()) {
        associated.setConstant(N, 4, std::numeric_limits<double>::quiet_NaN());
        for (int i = 0; i < N; ++i) {
            std::size_t rosbag_idx = pairs[i].second;
            
            associated.row(i) = rosbag_data.row(rosbag_idx);
            if (FLAGS_save_corrected)
                associated(i, 0) = rosbag_timestamps[rosbag_idx]; // corrected timestamp
        }
        
        const bool saved_rosbag_output = csv::write(associated, FLAGS_rosbag_output, 9);
        RUNTIME_ASSERT(saved_rosbag_output);
    }
    
    return 0;
}
