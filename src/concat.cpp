
#define PROGRAM_NAME \
    "concat"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, start_time, 0.0, "Rosbag start time [s]")                                    \
    FLAG_CASE(string, output_file, "concat.txt", "Output file for images associated")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input)

#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "util/macros.h"
#include "util/version.h"

namespace fs = boost::filesystem;

inline void read_file(const std::string &path, std::vector<double>& timestamps, std::vector<Eigen::Isometry3d>& poses ) {

    timestamps.clear();
    poses.clear();

    std::ifstream input(path);
    if (!input.is_open()) return;

    for (std::string line; std::getline(input, line);) {
        if (line.empty() || line.front() == '#') continue;

        std::istringstream iss(line);
        double timestamp, tx, ty, tz, qx, qy, qz, qw;
        if (iss >> timestamp >> tx >> ty >> tz >> qx >> qy >> qz >> qw) {
            timestamps.push_back(std::move(timestamp));
            
            Eigen::Vector3d t(tx, ty, tz);
            
            Eigen::Quaterniond q(qw, qx, qy, qz);
            q.normalize();
            
            Eigen::Isometry3d pose = Eigen::Isometry3d::Identity();
            pose.translation() = t;
            pose.linear() = q.toRotationMatrix();
            
            poses.push_back(std::move(pose));
        }
    }
}

void print_pose(std::ostream& stream, const double timestamp, const Eigen::Isometry3d& pose) {
    Eigen::Vector3d t(pose.translation());
    Eigen::Quaterniond q(pose.linear());
    q.normalize();
    
    std::stringstream ss;
    ss << std::fixed << std::setprecision(6)
       << timestamp << " "
       << t.x() << " "
       << t.y() << " "
       << t.z() << " "
       << q.x() << " "
       << q.y() << " "
       << q.z() << " "
       << q.w();
    stream << ss.str() << std::endl;
}

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_input));
}

inline void ValidateFlags() {
    RUNTIME_ASSERT(FLAGS_start_time >= 0.0);
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();
    
    std::vector<double> timestamps;
    std::vector<Eigen::Isometry3d> poses;
    read_file(ARGS_input, timestamps, poses);
    
    std::ofstream output_stream(FLAGS_output_file);
    RUNTIME_ASSERT(output_stream.is_open());
    
    Eigen::Isometry3d current_pose = Eigen::Isometry3d::Identity();
    print_pose(output_stream, FLAGS_start_time, current_pose);
    
    for (int i = 0; i < timestamps.size(); ++i) {
        const double& timestamp = timestamps.at(i);
        
        const Eigen::Isometry3d& relative_pose = poses.at(i);
        current_pose = current_pose * relative_pose;
        
        print_pose(output_stream, timestamp, current_pose);
    }

    return 0;
}