
#define PROGRAM_NAME \
    "extract_groundtruth"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(string, output_file, "groundtruth.txt", "Output file")

#define ARGS_CASES                                                                                 \
    ARG_CASE(input)

// STL
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include<Eigen/Core>
#include <Eigen/Geometry>

// ROS
#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <geographic_msgs/GeoPoseStamped.h>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"

namespace fs = boost::filesystem;

//57.29577951308232286464772187173366546630859375
//0.017453292519943295769236907684886127134428719

struct Ellipsoid {    
    enum class Model {
        WGS84,
        WGS72,
        GRS80,
        CLARKE1866
    };
    
    Ellipsoid(Model model) {
        switch (model) {
            case Model::WGS84:
                semimajor_axis = 6378137.0;
                flattening = 1.0 / 298.2572235630;
                semiminor_axis = semimajor_axis * (1.0 - flattening);
                break;
            case Model::WGS72:
                semimajor_axis = 6378135.0;
                flattening = 1.0 / 298.26;
                semiminor_axis = semimajor_axis * (1.0 - flattening);
                break;
            case Model::GRS80:
                semimajor_axis = 6378137.0;
                flattening = 1.0 / 298.257222100882711243;
                semiminor_axis = semimajor_axis * (1.0 - flattening);
                break;
            case Model::CLARKE1866:
                semimajor_axis = 6378206.4;
                semiminor_axis = 6356583.8;
                flattening = -(semiminor_axis / semimajor_axis - 1.0);
                break;
            default:
                throw std::invalid_argument("ellipsoid model not supported");
        }
    }

    double semimajor_axis;
    double semiminor_axis;
    double flattening;
};

inline double radians(const double deg) {
    return deg * 0.017453292519943295769236907684886127134428719;
}

inline Eigen::Matrix3d Rned_enu() {
    return (Eigen::MatrixXd(3, 3) <<  0.0,  1.0,  0.0,
                                      1.0,  0.0,  0.0,
                                      0.0,  0.0, -1.0)
           .finished();
}

Eigen::Matrix3d Renu_ecef(double lat, double lon,
                          const bool deg = true) {
    if (deg) {
        lat = radians(lat);
        lon = radians(lon);
    }

    const double slat = std::sin(lat);
    const double slon = std::sin(lon);
    const double clat = std::cos(lat);
    const double clon = std::cos(lon);

    Eigen::Matrix3d R;
    R <<         slon,         clon,  0.0,
         -slat * clon, -slat * slon, clat,
          clat * clon,  clat * slon, slat;

    return R;
}

Eigen::Matrix3d Rned_ecef(double lat, double lon,
                          const bool deg = true) {
    if (deg) {
        lat = radians(lat);
        lon = radians(lon);
    }

    const double slat = std::sin(lat);
    const double slon = std::sin(lon);
    const double clat = std::cos(lat);
    const double clon = std::cos(lon);

    Eigen::Matrix3d R;
    R << -slat * clon, -slat * slon,  clat,
                 slon,         clon,   0.0,
         -clat * clon, -clat * slon, -slat;
    
    return R;
}

Eigen::Vector3d uvw2enu(const double u, const double v, const double w,
                        double lat0, double lon0,
                        const bool deg = true) {
    if (deg) {
        lat0 = radians(lat0);
        lon0 = radians(lon0);
    }
    
    const double t =  std::cos(lon0) * u + std::sin(lon0) * v;
    const double E = -std::sin(lon0) * u + std::cos(lon0) * v;
    const double N = -std::sin(lat0) * t + std::cos(lat0) * w;
    const double U =  std::cos(lat0) * t + std::sin(lat0) * w;
    
    return Eigen::Vector3d(E, N, U); // e, n, u
}

Eigen::Vector3d geodetic2ecef(double lat, double lon, const double alt,
                              const Ellipsoid& ell = Ellipsoid(Ellipsoid::Model::WGS84),
                              const bool deg = true) {
    lat = deg ? radians(lat) : lat;
    lon = deg ? radians(lon) : lon;
    
    // radius of curvature of the prime vertical section
    const double N = std::pow(ell.semimajor_axis, 2) / std::sqrt(
                        std::pow(ell.semimajor_axis, 2) * std::pow(std::cos(lat), 2) +
                        std::pow(ell.semiminor_axis, 2) * std::pow(std::sin(lat), 2)
                     );
    
    // Compute cartesian (geocentric) coordinates given  (curvilinear) geodetic
    // coordinates.
    const double x = (N + alt) * std::cos(lat) * std::cos(lon);
    const double y = (N + alt) * std::cos(lat) * std::sin(lon);
    const double z = (N * std::pow(ell.semiminor_axis / ell.semimajor_axis, 2) + alt) * std::sin(lat);
    
    return Eigen::Vector3d(x, y, z);
}

Eigen::Vector3d geodetic2enu(const double lat, const double lon, const double h,
                             const double lat0, const double lon0, const double h0,
                             const Ellipsoid& ell = Ellipsoid(Ellipsoid::Model::WGS84),
                             const bool deg = true) {
    Eigen::Vector3d xyz1 = geodetic2ecef(lat, lon, h,
                                         ell, deg); // x, y, z
    Eigen::Vector3d xyz2 = geodetic2ecef(lat0, lon0, h0,
                                         ell, deg); // x, y, z
    Eigen::Vector3d diff = xyz1 - xyz2;
    return uvw2enu(diff.x(), diff.y(), diff.z(),
                   lat0, lon0,
                   deg); // e, n, u
}

Eigen::Vector3d geodetic2ned(const double lat, const double lon, const double h,
                             const double lat0, const double lon0, const double h0,
                             const Ellipsoid& ell = Ellipsoid(Ellipsoid::Model::WGS84),
                             const bool deg = true) {
    Eigen::Vector3d enu = geodetic2enu(lat, lon, h,
                                       lat0, lon0, h0,
                                       ell, deg); // e, n, u
    return Eigen::Vector3d(enu(1), enu(0), -enu(2)); // n, e, -u
}

static const std::string INS_REFERENCE = "/ins/reference";

inline std::string to_string(const double n, const int precision = 9) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(precision) << n;
    return ss.str();
}

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_input));
}

inline void ValidateFlags() {
    RUNTIME_ASSERT(!FLAGS_output_file.empty());
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();
    
    /*
    // Read ARGS_input (csv with ' ' separator)
    Eigen::MatrixXd gt_data = csv::read<double>(ARGS_input, ' ');
    // #id t_sec phi_rad lambda_rad h_m psi_rad theta_rad xi_rad saved
    RUNTIME_ASSERT(gt_data.cols() == 9);
    
    Eigen::MatrixXd data;
    static_assert(std::numeric_limits<double>::has_quiet_NaN);
    //data.setConstant(gt_data.rows(), 3, std::numeric_limits<double>::quiet_NaN());
    const int N = std::min(static_cast<int>(gt_data.rows()), 1500);
    data.setConstant(N, 3, std::numeric_limits<double>::quiet_NaN());
    const Ellipsoid wgs84 = Ellipsoid(Ellipsoid::Model::WGS84);
    */
    
    // Run geodetic2ned on each entry, with first entry as reference
    /*
    for (int i = 0; i < gt_data.rows(); ++i) {
        data.row(i) = geodetic2ned(gt_data(i, 2), gt_data(i, 3), gt_data(i, 4),
                                   gt_data(0, 2), gt_data(0, 3), gt_data(0, 4),
                                   wgs84, false)
                      .transpose();
    }
    */
    
    /*
    // Run geodetic2ecef in each entry, up to 1500
    for (int i = 0; i < N; ++i) {
        data.row(i) = geodetic2ecef(gt_data(i, 2), gt_data(i, 3), gt_data(i, 4),
                                    wgs84, false)
                      .transpose();
    }
    
    // Save output to FLAGS_output_file
    const bool saved_output = csv::write(data, FLAGS_output_file, 12);
    RUNTIME_ASSERT(saved_output);
    */
    
    const Ellipsoid wgs84 = Ellipsoid(Ellipsoid::Model::WGS84);
    
    std::ofstream output_stream(FLAGS_output_file);
    RUNTIME_ASSERT(output_stream.is_open());
    
    rosbag::Bag bag;
    bag.open(ARGS_input, rosbag::bagmode::Read);
    
    std::vector<std::string> topics = { INS_REFERENCE };
    rosbag::View view(bag, rosbag::TopicQuery(topics));
    
    Eigen::Matrix3d Rned0_ecef;
    geographic_msgs::GeoPoseStamped::ConstPtr init = nullptr;
    for(rosbag::MessageInstance const m: view) {
        geographic_msgs::GeoPoseStamped::ConstPtr msg = m.instantiate<geographic_msgs::GeoPoseStamped>();
        if (msg != nullptr) {
        /*
            output_stream << to_string(msg->header.stamp.toSec()) << " "
                          << to_string(radians(msg->pose.position.latitude), 9) << " "
                          << to_string(radians(msg->pose.position.longitude), 9) << " "
                          << to_string(msg->pose.position.altitude, 9) << " "
                          << std::endl;
        */
            if (init == nullptr) {
                init = msg;
                
                Rned0_ecef = Rned_ecef(init->pose.position.latitude,
                                       init->pose.position.longitude,
                                       true);
            }
            
            Eigen::Vector3d tned0_body = geodetic2ned(msg->pose.position.latitude,
                                                      msg->pose.position.longitude,
                                                      msg->pose.position.altitude,
                                                      init->pose.position.latitude,
                                                      init->pose.position.longitude,
                                                      init->pose.position.altitude,
                                                      wgs84, true);
            Eigen::Quaterniond qned_body(msg->pose.orientation.w,
                                         msg->pose.orientation.x,
                                         msg->pose.orientation.y,
                                         msg->pose.orientation.z);
            qned_body.normalize();
            
            Eigen::Matrix3d Recef_ned = Rned_ecef(msg->pose.position.latitude,
                                                  msg->pose.position.longitude,
                                                  true)
                                        .transpose();
            Eigen::Quaterniond qned0_ned(Rned0_ecef * Recef_ned);
            qned0_ned.normalize();
            
            Eigen::Quaterniond qned0_body = qned0_ned * qned_body;
            qned0_body.normalize();
            
            output_stream << to_string(msg->header.stamp.toSec()) << " "
                          << to_string(tned0_body.x()) << " "
                          << to_string(tned0_body.y()) << " "
                          << to_string(tned0_body.z()) << " "
                          << to_string(qned0_body.x()) << " "
                          << to_string(qned0_body.y()) << " "
                          << to_string(qned0_body.z()) << " "
                          << to_string(qned0_body.w())
                          << std::endl;
        }
    }
    
    output_stream.close();
    
    bag.close();
    
    return 0;
}
