
#define PROGRAM_NAME \
    "absolute"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(double, time_offset, 0.0, "Time offset to be added to trajectory")                   \
    FLAG_CASE(string, output_reference, "output_reference.csv", "Output reference trajectory")     \
    FLAG_CASE(string, output_trajectory, "output_trajectory.csv", "Output trajectory")

#define ARGS_CASES                                                                                 \
    ARG_CASE(reference)                                                                            \
    ARG_CASE(trajectory)

#include <cmath>
#include <fstream>
#include <limits>
#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

#include "associate.hpp"
#include "csv.hpp"

#include "util/macros.h"
#include "util/version.h"

namespace fs = boost::filesystem;

struct Ellipsoid {    
    enum class Model {
        WGS84,
        WGS72,
        GRS80,
        CLARKE1866
    };
    
    Ellipsoid(Model model) {
        switch (model) {
            case Model::WGS84:
                semimajor_axis = 6378137.0;
                flattening = 1.0 / 298.2572235630;
                semiminor_axis = semimajor_axis * (1.0 - flattening);
                break;
            case Model::WGS72:
                semimajor_axis = 6378135.0;
                flattening = 1.0 / 298.26;
                semiminor_axis = semimajor_axis * (1.0 - flattening);
                break;
            case Model::GRS80:
                semimajor_axis = 6378137.0;
                flattening = 1.0 / 298.257222100882711243;
                semiminor_axis = semimajor_axis * (1.0 - flattening);
                break;
            case Model::CLARKE1866:
                semimajor_axis = 6378206.4;
                semiminor_axis = 6356583.8;
                flattening = -(semiminor_axis / semimajor_axis - 1.0);
                break;
            default:
                throw std::invalid_argument("ellipsoid model not supported");
        }
    }

    double semimajor_axis;
    double semiminor_axis;
    double flattening;
};

inline double radians(const double deg) {
    return deg * 0.017453292519943295769236907684886127134428719;
}

inline Eigen::Matrix3d Rned_enu() {
    return (Eigen::MatrixXd(3, 3) <<  0.0,  1.0,  0.0,
                                      1.0,  0.0,  0.0,
                                      0.0,  0.0, -1.0)
           .finished();
}

Eigen::Matrix3d Renu_ecef(double lat, double lon,
                          const bool deg = true) {
    if (deg) {
        lat = radians(lat);
        lon = radians(lon);
    }

    const double slat = std::sin(lat);
    const double slon = std::sin(lon);
    const double clat = std::cos(lat);
    const double clon = std::cos(lon);

    Eigen::Matrix3d R;
    R <<         slon,         clon,  0.0,
         -slat * clon, -slat * slon, clat,
          clat * clon,  clat * slon, slat;

    return R;
}

Eigen::Matrix3d Rned_ecef(double lat, double lon,
                          const bool deg = true) {
    if (deg) {
        lat = radians(lat);
        lon = radians(lon);
    }

    const double slat = std::sin(lat);
    const double slon = std::sin(lon);
    const double clat = std::cos(lat);
    const double clon = std::cos(lon);

    Eigen::Matrix3d R;
    R << -slat * clon, -slat * slon,  clat,
                 slon,         clon,   0.0,
         -clat * clon, -clat * slon, -slat;
    
    return R;
}

Eigen::Vector3d uvw2enu(const double u, const double v, const double w,
                        double lat0, double lon0,
                        const bool deg = true) {
    if (deg) {
        lat0 = radians(lat0);
        lon0 = radians(lon0);
    }
    
    const double t =  std::cos(lon0) * u + std::sin(lon0) * v;
    const double E = -std::sin(lon0) * u + std::cos(lon0) * v;
    const double N = -std::sin(lat0) * t + std::cos(lat0) * w;
    const double U =  std::cos(lat0) * t + std::sin(lat0) * w;
    
    return Eigen::Vector3d(E, N, U); // e, n, u
}

Eigen::Vector3d geodetic2ecef(double lat, double lon, const double alt,
                              const Ellipsoid& ell = Ellipsoid(Ellipsoid::Model::WGS84),
                              const bool deg = true) {
    lat = deg ? radians(lat) : lat;
    lon = deg ? radians(lon) : lon;
    
    // radius of curvature of the prime vertical section
    const double N = std::pow(ell.semimajor_axis, 2) / std::sqrt(
                        std::pow(ell.semimajor_axis, 2) * std::pow(std::cos(lat), 2) +
                        std::pow(ell.semiminor_axis, 2) * std::pow(std::sin(lat), 2)
                     );
    
    // Compute cartesian (geocentric) coordinates given  (curvilinear) geodetic
    // coordinates.
    const double x = (N + alt) * std::cos(lat) * std::cos(lon);
    const double y = (N + alt) * std::cos(lat) * std::sin(lon);
    const double z = (N * std::pow(ell.semiminor_axis / ell.semimajor_axis, 2) + alt) * std::sin(lat);
    
    return Eigen::Vector3d(x, y, z);
}

Eigen::Vector3d geodetic2enu(const double lat, const double lon, const double h,
                             const double lat0, const double lon0, const double h0,
                             const Ellipsoid& ell = Ellipsoid(Ellipsoid::Model::WGS84),
                             const bool deg = true) {
    Eigen::Vector3d xyz1 = geodetic2ecef(lat, lon, h,
                                         ell, deg); // x, y, z
    Eigen::Vector3d xyz2 = geodetic2ecef(lat0, lon0, h0,
                                         ell, deg); // x, y, z
    Eigen::Vector3d diff = xyz1 - xyz2;
    return uvw2enu(diff.x(), diff.y(), diff.z(),
                   lat0, lon0,
                   deg); // e, n, u
}

Eigen::Vector3d geodetic2ned(const double lat, const double lon, const double h,
                             const double lat0, const double lon0, const double h0,
                             const Ellipsoid& ell = Ellipsoid(Ellipsoid::Model::WGS84),
                             const bool deg = true) {
    Eigen::Vector3d enu = geodetic2enu(lat, lon, h,
                                       lat0, lon0, h0,
                                       ell, deg); // e, n, u
    return Eigen::Vector3d(enu(1), enu(0), -enu(2)); // n, e, -u
}

Eigen::Isometry3d relative_pose(Eigen::MatrixXd& data, const std::size_t idx1, const std::size_t idx2) {
    Eigen::Quaterniond q1(data(idx1, 7), data(idx1, 4), data(idx1, 5), data(idx1, 6));
    q1.normalize();
    
    Eigen::Isometry3d pose1;
    pose1.linear() = q1.toRotationMatrix();
    pose1.translation() = data.block<1, 3>(idx1, 1).transpose();
    
    Eigen::Quaterniond q2(data(idx2, 7), data(idx2, 4), data(idx2, 5), data(idx2, 6));
    q2.normalize();
    
    Eigen::Isometry3d pose2;
    pose2.linear() = q2.toRotationMatrix();
    pose2.translation() = data.block<1, 3>(idx2, 1).transpose();
    
    return pose1.inverse() * pose2;
}

inline std::string to_string(const double n, const int precision = 9) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(precision) << n;
    return ss.str();
}

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_reference));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_trajectory));
}

inline void ValidateFlags() {
    RUNTIME_ASSERT(!FLAGS_output_reference.empty());
    RUNTIME_ASSERT(!FLAGS_output_trajectory.empty());
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();
    
    // Read ARGS_reference (csv with ' ' separator)
    Eigen::MatrixXd reference_data = csv::read<double>(ARGS_reference, ' ');
    // #ts phi_rad lambda_rad h_m
    RUNTIME_ASSERT(reference_data.cols() == 8);
    
    double max_difference = std::numeric_limits<double>::max();
    
    std::vector<double> reference_timestamps(reference_data.rows());
    for (int i = 0; i < reference_data.rows(); ++i) {
        reference_timestamps[i] = reference_data(i, 0);
        if (i > 0) {
            const double difference = reference_timestamps[i] - reference_timestamps[i-1];
            RUNTIME_ASSERT(difference > 0.0);
            if (difference < max_difference) max_difference = difference;
        }
    }
    
    // Read ARGS_reference (csv with ' ' separator)
    Eigen::MatrixXd trajectory_data = csv::read<double>(ARGS_trajectory, ' ');
    // #ts phi_rad lambda_rad h_m
    RUNTIME_ASSERT(trajectory_data.cols() == 8);

    std::vector<double> trajectory_timestamps(trajectory_data.rows());
    for (int i = 0; i < trajectory_data.rows(); ++i) {
        trajectory_timestamps[i] = trajectory_data(i, 0) + FLAGS_time_offset;
        if (i > 0) {
            const double difference = reference_timestamps[i] - reference_timestamps[i-1];
            RUNTIME_ASSERT(difference > 0.0);
            if (difference < max_difference) max_difference = difference;
        }
    }
    
    //max_difference /= 2.0;
    
    // Compute pose associations
    std::vector<std::pair<std::size_t, std::size_t>> pairs = associate(reference_timestamps, trajectory_timestamps, 0.001);
    const int N = pairs.size();
    RUNTIME_ASSERT(N >= 3);
    
    const Ellipsoid wgs84 = Ellipsoid(Ellipsoid::Model::WGS84);
    
    std::ofstream output_reference(FLAGS_output_reference);
    RUNTIME_ASSERT(output_reference.is_open());
    
    std::ofstream output_trajectory(FLAGS_output_trajectory);
    RUNTIME_ASSERT(output_trajectory.is_open());

    const std::size_t init_idx = pairs.front().first;
    
    const double lat0 = reference_data(init_idx, 1);
    const double lon0 = reference_data(init_idx, 2);
    const double h0   = reference_data(init_idx, 3);
    
    Eigen::Matrix3d Rned0_ecef = Rned_ecef(reference_data(init_idx, 1),
                                           reference_data(init_idx, 2),
                                           false);
    
    Eigen::Quaterniond qned0_body = Eigen::Quaterniond(reference_data(init_idx, 4),
                                                       reference_data(init_idx, 5),
                                                       reference_data(init_idx, 6),
                                                       reference_data(init_idx, 7));
    qned0_body.normalize();
    
    Eigen::Isometry3d Tbody0_ned0 = Eigen::Isometry3d::Identity();
    Tbody0_ned0.linear() = qned0_body.toRotationMatrix().transpose();
    
    Eigen::Isometry3d Tned0_body0 = Tbody0_ned0.inverse();
    
    // TODO Orientation
    for (int i = 1; i < N; ++i) {
        const std::size_t reference_idx = pairs[i].first;
        
        Eigen::Vector3d tned0_body = geodetic2ned(reference_data(reference_idx, 1),
                                                  reference_data(reference_idx, 2),
                                                  reference_data(reference_idx, 3),
                                                  lat0, lon0, h0,
                                                  wgs84, false);
        Eigen::Quaterniond qned_body(reference_data(reference_idx, 4),
                                     reference_data(reference_idx, 5),
                                     reference_data(reference_idx, 6),
                                     reference_data(reference_idx, 7));
        qned_body.normalize();
        
        Eigen::Matrix3d Recef_ned = Rned_ecef(reference_data(reference_idx, 1),
                                              reference_data(reference_idx, 2),
                                              false)
                                    .transpose();
        Eigen::Quaterniond qned0_ned(Rned0_ecef * Recef_ned);
        qned0_ned.normalize();
        
        Eigen::Quaterniond qned0_body = qned0_ned * qned_body;
        qned0_body.normalize();
        
        Eigen::Isometry3d Tned0_body;
        Tned0_body.linear() = qned0_body.toRotationMatrix();
        Tned0_body.translation() = tned0_body;
        
        //Eigen::Isometry3d Tbody0_body = Tbody0_ned0 * Tned0_body;
        
        /*
        output_reference << to_string(Tbody0_body.translation().x(), 9) << ","
                         << to_string(Tbody0_body.translation().y(), 9) << ","
                         << to_string(Tbody0_body.translation().z(), 9)
                         << std::endl;
        */
        output_reference << to_string(Tned0_body.translation().x(), 9) << ","
                         << to_string(Tned0_body.translation().y(), 9) << ","
                         << to_string(Tned0_body.translation().z(), 9)
                         << std::endl;
        
        const std::size_t trajectory_idx = pairs[i].second;
        
        Eigen::Vector3d tbody0_body(trajectory_data(trajectory_idx, 1),
                                    trajectory_data(trajectory_idx, 2),
                                    trajectory_data(trajectory_idx, 3));
        Eigen::Quaterniond qbody0_body(trajectory_data(trajectory_idx, 7),
                                       trajectory_data(trajectory_idx, 4),
                                       trajectory_data(trajectory_idx, 5),
                                       trajectory_data(trajectory_idx, 6));
        qbody0_body.normalize();
        
        Eigen::Isometry3d Tbody0_body_;
        Tbody0_body_.linear() = qbody0_body.toRotationMatrix();
        Tbody0_body_.translation() = tbody0_body;
        
        Eigen::Isometry3d Tned0_body_ = Tned0_body0 * Tbody0_body_;
        
        output_trajectory << to_string(Tned0_body_.translation().x(), 9) << ","
                          << to_string(Tned0_body_.translation().y(), 9) << ","
                          << to_string(Tned0_body_.translation().z(), 9)
                          << std::endl;
    }

    output_reference.close();
    output_trajectory.close();

    return 0;
}