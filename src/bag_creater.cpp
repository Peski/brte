
#define PROGRAM_NAME \
    "bag_creater"

#define FLAGS_CASES                                                                                \
    FLAG_CASE(bool, grayscale, false, "Convert images to grayscale")                               \
    FLAG_CASE(double, time_origin, -1.0, "New time origin [s]")

#define ARGS_CASES                                                                                 \
    ARG_CASE(truth_file)                                                                           \
    ARG_CASE(sens_file)                                                                            \
    ARG_CASE(images_file)                                                                          \
    ARG_CASE(images_dir)                                                                           \
    ARG_CASE(output)

// STL
#include <algorithm>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

// Args (Gflags extension)
#include "args.hpp"

// Boost
#include <boost/filesystem/operations.hpp>
#include <boost/range/iterator_range.hpp>

// Eigen
#include <Eigen/Core>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

// ROS
#include <ros/ros.h>
#include <rosbag/bag.h>

#include <geographic_msgs/GeoPoseStamped.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>

#include <cv_bridge/cv_bridge.h>

#include "util/macros.h"
#include "util/version.h"

#include "csv.hpp"
#include "sequence.hpp"

namespace fs = boost::filesystem;

static const std::string INS_REFERENCE = "/ins/reference";
static const std::string CAM_IMAGE = "/camera/image_raw";
static const std::string IMU_DATA = "/imu/data";

inline double RadToDeg(const double rad) {
    return rad * 57.29577951308232286464772187173366546630859375;
}

inline std::string to_string(int n, int w = 0) {
    std::stringstream ss;
    ss << std::setw(w) << std::setfill('0') << n;
    return ss.str();
}

inline std::string to_string(const double n, const int precision = 9) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(precision) << n;
    return ss.str();
}

inline void ValidateArgs() {
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_truth_file));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_sens_file));
    RUNTIME_ASSERT(fs::is_regular_file(ARGS_images_file));
    RUNTIME_ASSERT(fs::is_directory(ARGS_images_dir));
}

inline void ValidateFlags() {
    RUNTIME_ASSERT(std::isfinite(FLAGS_time_origin));
}

int main(int argc, char* argv[]) {

    // Print build info
    std::cout << PROGRAM_NAME << " (" << GetBuildInfo() << ")" << std::endl;
    std::cout << std::endl;

    // Handle help flag
    if (args::HelpRequired(argc, argv)) {
        args::ShowHelp();
        return 0;
    }

    // Parse input flags
    args::ParseCommandLineNonHelpFlags(&argc, &argv, true);

    // Check number of args
    if (argc-1 != args::NumArgs()) {
        args::ShowHelp();
        return -1;
    }

    // Parse input args
    args::ParseCommandLineArgs(argc, argv);

    // Validate input arguments
    ValidateFlags();
    ValidateArgs();

    std::cout << "Time min: " << ros::TIME_MIN << std::endl;
    const double base_sec = (FLAGS_time_origin >= 0.0) ? FLAGS_time_origin : static_cast<double>(std::time(nullptr));
    std::cout << "New time origin: " << to_string(base_sec) << std::endl;
    
    rosbag::Bag bag;
    bag.open(ARGS_output, rosbag::bagmode::Write);
    
    // Read file
    std::cout << "Reading file " << ARGS_truth_file << std::endl;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> gt_data =
        csv::read<double, Eigen::RowMajor>(ARGS_truth_file, ' ');
    RUNTIME_ASSERT(gt_data.cols() == 8); // 8 columns expected
    RUNTIME_ASSERT(gt_data.rows() > 0); // invalid file?
    
    // Sort by time offset
    //  (we sort row pointers to avoid copying the whole matrix)
    std::vector<double*> gt_rowwise;
    gt_rowwise.reserve(gt_data.rows());
    for (int i = 0; i < gt_data.rows(); ++i)
        gt_rowwise.push_back(gt_data.data() + 8*i);

    std::sort(gt_rowwise.begin(), gt_rowwise.end(),
        [](double* lhs, double* rhs) {
            return lhs[0] < rhs[0];
        });
    
    int ctr = 1;
    int w = std::to_string(gt_data.rows()).size();
    for (double *row_data : gt_rowwise) {
        std::cout << '\r' << to_string(ctr, w) << " / " << gt_data.rows() << std::flush;
        
        geographic_msgs::GeoPoseStamped gt_pose;
        
        // Header
        gt_pose.header.seq = ctr - 1;
        gt_pose.header.stamp = ros::Time(base_sec + row_data[0]); // s
        
        // Pose
        //  Position
        gt_pose.pose.position.latitude = RadToDeg(row_data[1]); // deg
        gt_pose.pose.position.longitude = RadToDeg(row_data[2]); // deg
        gt_pose.pose.position.altitude = row_data[3]; // m
        
        //  Orientation
        gt_pose.pose.orientation.x = row_data[5];
        gt_pose.pose.orientation.y = row_data[6];
        gt_pose.pose.orientation.z = row_data[7];
        gt_pose.pose.orientation.w = row_data[4];
        
        bag.write(INS_REFERENCE, gt_pose.header.stamp, gt_pose);
        ++ctr;
    }
    std::cout << std::endl;
    
    // Free memory
    gt_rowwise.clear();
    gt_data.resize(0, 0);
    
    // Read file
    std::cout << "Reading file " << ARGS_sens_file << std::endl;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> imu_data =
        csv::read<double, Eigen::RowMajor>(ARGS_sens_file, ' '); // RowMajor to get continuous rows in memory
    RUNTIME_ASSERT(imu_data.cols() == 7); // 7 columns expected
    RUNTIME_ASSERT(imu_data.rows() > 0); // invalid file?
    
    // Sort by time offset (row pointers)
    std::vector<double*> imu_rowwise;
    imu_rowwise.reserve(imu_data.rows());
    for (int i = 0; i < imu_data.rows(); ++i)
        imu_rowwise.emplace_back(imu_data.data() + 7*i);

    std::sort(imu_rowwise.begin(), imu_rowwise.end(),
        [](double* lhs, double* rhs) {
            return lhs[0] < rhs[0];
        });
    
    ctr = 1;
    w = std::to_string(imu_data.rows()).size();
    for (double *row_data : imu_rowwise) {
        std::cout << '\r' << to_string(ctr, w) << " / " << imu_data.rows() << std::flush;
        
        sensor_msgs::Imu imu_measurement;
        
        // Header
        imu_measurement.header.seq = ctr - 1;
        imu_measurement.header.stamp = ros::Time(base_sec + row_data[0]);
        
        // Orientation
        imu_measurement.orientation_covariance[0] = -1.0;
        
        // Angular velocity
        imu_measurement.angular_velocity.x = row_data[4];
        imu_measurement.angular_velocity.y = row_data[5];
        imu_measurement.angular_velocity.z = row_data[6];
        for (int k = 0; k < 9; ++k)
            imu_measurement.angular_velocity_covariance[k] = 0.0;
        
        // Linear acceleration
        imu_measurement.linear_acceleration.x = row_data[1];
        imu_measurement.linear_acceleration.y = row_data[2];
        imu_measurement.linear_acceleration.z = row_data[3];
        for (int k = 0; k < 9; ++k)
            imu_measurement.linear_acceleration_covariance[k] = 0.0;

        bag.write(IMU_DATA, imu_measurement.header.stamp, imu_measurement);
        ++ctr;
    }
    std::cout << std::endl;
    
    // Free memory
    imu_rowwise.clear();
    imu_data.resize(0, 0);
    
    std::cout << "Reading file " << ARGS_images_file << std::endl;
    Eigen::MatrixXd images_data = csv::read<double>(ARGS_images_file, ' ');
    RUNTIME_ASSERT(images_data.cols() == 9); // 9 columns expected
    RUNTIME_ASSERT(images_data.rows() > 0); // invalid file?
    
    std::unordered_map<int, double> id2offset;
    for (int i = 0; i < images_data.rows(); ++i)
        id2offset[images_data(i, 0)] = images_data(i, 1);
    
    std::cout << "Reading images from " << ARGS_images_file << std::endl;
    std::vector<std::string> seq = sequence::getSequence(ARGS_images_dir);
    
    ctr = 1;
    w = std::to_string(seq.size()).size();
    for (int i = 0; i < seq.size(); ++i) {
        std::cout << '\r' << to_string(ctr, w) << " / " << seq.size() << std::flush;
        ++ctr;
        
        const std::string& file = seq[i];
        const int id = sequence::sequence_id::get_sid(seq[i]);
        std::unordered_map<int, double>::const_iterator cit = id2offset.find(id);
        if (cit == id2offset.cend()) {
            fs::path image_path = file;
            std::cout << "[WARNING] Skipping image " << image_path.filename().string() << std::endl;
            continue;
        }
        
        // Header
        std_msgs::Header header;
        header.seq = id;
        header.stamp = ros::Time(base_sec + cit->second);
        
        cv::Mat image = cv::imread(file, CV_LOAD_IMAGE_COLOR);
        
        sensor_msgs::ImagePtr cam_image;
        if (FLAGS_grayscale) {
            cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
            cam_image = cv_bridge::CvImage(header, "mono8", image).toImageMsg();
        } else {
            cam_image = cv_bridge::CvImage(header, "bgr8", image).toImageMsg();
        }
        
        bag.write(CAM_IMAGE, cam_image->header.stamp, *cam_image);
    }
    std::cout << std::endl;
    
    bag.close();

    return 0;
}

